// Created by Paulius Cesekas on 14/04/2018.

import Foundation
import Domain
import CoreLocation

public class MockData {
    private static let lithuania = Region(
        name: "Lithuania",
        southWest: CLLocationCoordinate2D(
            latitude: 54.09965,
            longitude: 21.00641),
        northEast: CLLocationCoordinate2D(
            latitude: 56.39798,
            longitude: 26.59120),
        zoom: Zoom(
            minimum: 5,
            maximum: 10))
    private static let kaunoMarios = Region(
        name: "Kauno marios",
        southWest: CLLocationCoordinate2D(
            latitude: 54.56162,
            longitude: 24.00558),
        northEast: CLLocationCoordinate2D(
            latitude: 54.94236,
            longitude: 24.31458),
        zoom: Zoom(
            minimum: 5,
            maximum: 15))

    public var regions: [Region] {
        return [MockData.lithuania, MockData.kaunoMarios]
    }
    
    private static var slienavaRoute: Route {
        let route = Route(name: "Kauno marios - Šlienava")
        route.addCheckpoint(Checkpoint(
            name: "Kauno marios",
            coordinate: CLLocationCoordinate2D(
                latitude: 54.872654,
                longitude: 24.124895)))
        route.addCheckpoint(Checkpoint(
            name: "Šlienava",
            coordinate: CLLocationCoordinate2D(
                latitude: 54.864455,
                longitude: 24.113096)))
        route.addCheckpoint(Checkpoint(
            name: "J. Biliūno gatvė 10",
            coordinate: CLLocationCoordinate2D(
                latitude: 54.859715,
                longitude: 24.108125)))
        route.addCheckpoint(Checkpoint(
            name: "Dubrava",
            coordinate: CLLocationCoordinate2D(
                latitude: 54.842200,
                longitude: 24.098107)))
        route.addCheckpoint(Checkpoint(
            name: "Gervėnupis",
            coordinate: CLLocationCoordinate2D(
                latitude: 54.835823,
                longitude: 24.113224)))
        return route
    }
    
    public var kaunoMariosRoutes: [Route] {
        return [MockData.slienavaRoute]
    }

}
