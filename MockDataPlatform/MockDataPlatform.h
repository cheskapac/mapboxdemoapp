//
//  MockDataPlatform.h
//  MockDataPlatform
//
//  Created by Paulius Cesekas on 13/04/2018.
//  Copyright © 2018 cheskapac. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for MockDataPlatform.
FOUNDATION_EXPORT double MockDataPlatformVersionNumber;

//! Project version string for MockDataPlatform.
FOUNDATION_EXPORT const unsigned char MockDataPlatformVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MockDataPlatform/PublicHeader.h>


