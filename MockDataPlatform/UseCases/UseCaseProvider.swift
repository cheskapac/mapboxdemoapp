// Created by Paulius Cesekas on 15/04/2018.

import Foundation
import Domain

public final class UseCaseProvider: Domain.UseCaseProvider {
    private let mockDataProvider: MockDataProvider
    
    public init() {
        mockDataProvider = MockDataProvider()
    }

    public func makeNavigationRouteUseCase() -> Domain.NavigationRouteUseCase {
        let mockData = mockDataProvider.makeMockData()
        return NavigationRouteUseCase(mockData: mockData)
    }
}
