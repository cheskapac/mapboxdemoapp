// Created by Paulius Cesekas on 15/04/2018.

import Foundation
import Domain
import RxSwift

final class NavigationRouteUseCase: Domain.NavigationRouteUseCase {
    private let mockData: MockData
    
    init(mockData: MockData) {
        self.mockData = mockData
    }

    func regions() -> Observable<[Region]> {
        let regions = mockData.regions
        return Observable<[Region]>.just(regions)
    }
    
    func routes(for region: Region) -> Observable<[Route]> {
        guard region.name == "Kauno marios" else {
            return Observable<[Route]>.just([])
        }
        
        let routes = mockData.kaunoMariosRoutes
        return Observable<[Route]>.just(routes)
    }
}

struct MapFromNever: Error {}
extension ObservableType where E == Never {
    func map<T>(to: T.Type) -> Observable<T> {
        return self.flatMap { _ in
            return Observable<T>.error(MapFromNever())
        }
    }
}
