//  Created by Paulius Cesekas on 13/04/2018.

import Foundation
import RxSwift

public protocol NavigationRouteUseCase {
    func regions() -> Observable<[Region]>
    func routes(for region: Region) -> Observable<[Route]>
}
