//  Copyright © 2018 cheskapac. All rights reserved.

import Foundation

public protocol UseCaseProvider {
    func makeNavigationRouteUseCase() -> NavigationRouteUseCase
}
