//  Created by Paulius Cesekas on 13/04/2018.

import CoreLocation

public struct Checkpoint {
    public let name: String?
    public let coordinate: CLLocationCoordinate2D
    
    public init(
        name: String?,
        coordinate: CLLocationCoordinate2D) {
            self.name = name
            self.coordinate = coordinate
    }
}
