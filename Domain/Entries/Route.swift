//  Created by Paulius Cesekas on 13/04/2018.

import Foundation

public class Route {
    public var name: String
    public var checkpointList: [Checkpoint]
    
    public init(name: String) {
        self.name = name
        checkpointList = [Checkpoint]()
    }
}

extension Route {
    public func addCheckpoint(_ checkpoint: Checkpoint) {
        checkpointList.append(checkpoint)
    }
}
