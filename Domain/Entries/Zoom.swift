// Created by Paulius Cesekas on 14/04/2018.

import Foundation

public struct Zoom {
    public let minimum: Int
    public let maximum: Int
    
    public init(
        minimum: Int,
        maximum: Int) {
            self.minimum = minimum
            self.maximum = maximum
    }
}
