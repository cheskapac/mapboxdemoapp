//  Created by Paulius Cesekas on 13/04/2018.

import CoreLocation

public struct Region {
    public let name: String
    public let southWest: CLLocationCoordinate2D
    public let northEast: CLLocationCoordinate2D
    public let zoom: Zoom
    
    public init(
        name: String,
        southWest: CLLocationCoordinate2D,
        northEast: CLLocationCoordinate2D,
        zoom: Zoom) {
            self.name = name
            self.southWest = southWest
            self.northEast = northEast
            self.zoom = zoom
    }
}

extension Region: Hashable {
    public var hashValue: Int {
        return name.hashValue
    }
    
    public static func == (lhs: Region, rhs: Region) -> Bool {
        return lhs.name == rhs.name
            && lhs.southWest.latitude == rhs.southWest.latitude
            && lhs.southWest.longitude == rhs.southWest.longitude
            && lhs.northEast.latitude == rhs.northEast.latitude
            && lhs.northEast.longitude == rhs.northEast.longitude
            && lhs.zoom.minimum == rhs.zoom.maximum
            && lhs.zoom.maximum == rhs.zoom.maximum
    }
    
    
}
