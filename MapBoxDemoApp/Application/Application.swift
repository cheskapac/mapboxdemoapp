// Created by Paulius Cesekas on 15/04/2018.

import Domain
import MockDataPlatform

final class Application {
    static let shared = Application()
    
    private let mockDataProvider: Domain.UseCaseProvider
    
    private init() {
        mockDataProvider = MockDataPlatform.UseCaseProvider()
    }
    
    func configureInitialInterface(in window: UIWindow) {
        let navigationController = UINavigationController()
        window.rootViewController = navigationController
        
        let storyboard = StoryboardScene.NavigationRoute.storyboard
        let navigator = DefaultNavigationRouteNavigator(
            services: mockDataProvider,
            navigationController: navigationController,
            storyBoard: storyboard)
        navigator.navigateToMap()
    }
}
