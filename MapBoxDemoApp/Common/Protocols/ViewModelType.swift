//  Created by Paulius Cesekas on 14/04/2018.

protocol ViewModelType {
    associatedtype Input
    associatedtype Output
    
    func transform(input: Input) -> Output
}
