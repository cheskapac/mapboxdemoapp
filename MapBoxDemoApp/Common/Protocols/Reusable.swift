// Created by Paulius Cesekas on 10/04/2018.

import Foundation

protocol Reusable {
    static var reuseIdentifier: String { get }
}
