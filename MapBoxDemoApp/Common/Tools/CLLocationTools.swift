// Created by Paulius Cesekas on 20/04/2018.

import Foundation
import CoreLocation

extension CLLocationCoordinate2D {
    static func degreeToRadian(angle: CLLocationDegrees) -> Double {
        return (angle) / 180.0 * Double.pi
    }

    static func radianToDegree(radian: Double) -> CLLocationDegrees {
        return CLLocationDegrees(radian * (180.0 / Double.pi))
    }

    static func middlePointOfListMarkers(listCoords: [CLLocationCoordinate2D]) -> CLLocationCoordinate2D {
        var x = 0.0
        var y = 0.0
        var z = 0.0

        for coordinate in listCoords{
            let lat = degreeToRadian(angle: coordinate.latitude)
            let lon = degreeToRadian(angle: coordinate.longitude)
            x = x + cos(lat) * cos(lon)
            y = y + cos(lat) * sin(lon);
            z = z + sin(lat);
        }
        
        x = x / Double(listCoords.count)
        y = y / Double(listCoords.count)
        z = z / Double(listCoords.count)
        
        let resultLon = atan2(y, x)
        let resultHyp = sqrt(x * x + y * y)
        let resultLat = atan2(z, resultHyp)
        let newLat = radianToDegree(radian: resultLat)
        let newLon = radianToDegree(radian: resultLon)
        let result = CLLocationCoordinate2D(latitude: newLat, longitude: newLon)
        return result
    }
}
