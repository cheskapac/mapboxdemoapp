//  Created by Paulius Cesekas on 09/04/2018.

import UIKit

enum Log {
    static func d(
        _ message: String,
        file: String = #file,
        function: String = #function,
        line: Int = #line) {
        #if DEBUG
        let fileName = file.components(separatedBy: "/").last ?? file
        let className = (fileName as NSString).deletingPathExtension
        let threadIdentifier = (Thread.isMainThread) ? "M" : "B"
        NSLog("[\(className).\(function) \(line) <\(threadIdentifier)>]: \(message)")
        #endif
    }
}

func loadXibFromClass(_ aClass: AnyClass) -> UIView {
    guard let xibFromClass = Bundle.main.loadNibNamed(
        classNameFromClass(aClass),
        owner: nil,
        options: nil)?.first as? UIView else {
            print("ERROR! Could not load xib named \(aClass)")
            return UIView()
    }
    return xibFromClass
}

func classNameFromClass(_ aClass: AnyClass) -> String {
    guard let callClassName: String = NSStringFromClass(aClass)
        .components(separatedBy: ".")
        .last else {
            print("ERROR! could not get class name from class: \(aClass)")
            return ""
    }
    return callClassName
}
