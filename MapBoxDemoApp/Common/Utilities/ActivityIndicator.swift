//  Created by Paulius Cesekas on 10/04/2018.

import Foundation
import RxSwift
import RxCocoa

public class ActivityIndicator: SharedSequenceConvertibleType {
    // swiftlint:disable type_name
    public typealias E = Bool
    // swiftlint:enable type_name
    public typealias SharingStrategy = DriverSharingStrategy
    
    private let recursiveLock = NSRecursiveLock()
    private var variable = BehaviorRelay(value: false)
    private let loading: SharedSequence<SharingStrategy, Bool>
    
    public init() {
        loading = variable.asDriver()
            .distinctUntilChanged()
    }
    
    fileprivate func trackActivityOfObservable<O: ObservableConvertibleType>(_ source: O) -> Observable<O.E> {
        return source.asObservable()
            .do(onNext: { _ in
                self.sendStopLoading()
            }, onError: { _ in
                self.sendStopLoading()
            }, onCompleted: {
                self.sendStopLoading()
            }, onSubscribe: subscribed)
    }
    
    private func subscribed() {
        recursiveLock.lock()
        variable.accept(true)
        recursiveLock.unlock()
    }
    
    private func sendStopLoading() {
        recursiveLock.lock()
        variable.accept(false)
        recursiveLock.unlock()
    }
    
    public func asSharedSequence() -> SharedSequence<SharingStrategy, E> {
        return loading
    }
}

extension ObservableConvertibleType {
    public func trackActivity(_ activityIndicator: ActivityIndicator) -> Observable<E> {
        return activityIndicator.trackActivityOfObservable(self)
    }
}
