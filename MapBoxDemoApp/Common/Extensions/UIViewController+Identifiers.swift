//  Created by Paulius Cesekas on 09/04/2018.

import UIKit

extension UIViewController {
    static var identifier: String {
        return classNameFromClass(self)
    }
}
