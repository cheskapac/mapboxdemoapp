//  Created by Paulius Cesekas on 09/04/2018.

import UIKit

extension UIStoryboard {
    func instantiateViewController<T: UIViewController>() -> T? {
        guard let viewController = instantiateViewController(withIdentifier: T.identifier) as? T else {
            fatalError()
        }
        return viewController
    }
    
    func instantiateViewController<T: UIViewController>(ofType type: T.Type = T.self) -> T {
        guard let viewController = instantiateViewController(withIdentifier: T.identifier) as? T else {
            fatalError()
        }
        return viewController
    }
}
