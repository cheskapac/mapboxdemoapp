//  Created by Paulius Cesekas on 10/04/2018.

import UIKit

extension UITableViewCell: Reusable {
    static var reuseIdentifier: String {
        return classNameFromClass(self)
    }
}
