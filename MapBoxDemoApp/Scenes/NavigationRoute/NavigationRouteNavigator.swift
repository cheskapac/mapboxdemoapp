//  Created by Paulius Cesekas on 13/04/2018.

import Foundation
import Domain

protocol NavigationRouteNavigator {
    func navigateToMap()
}

class DefaultNavigationRouteNavigator: NavigationRouteNavigator {
    private let storyboard: UIStoryboard
    private let navigationController: UINavigationController
    private let services: UseCaseProvider
    
    init(services: UseCaseProvider,
         navigationController: UINavigationController,
         storyBoard: UIStoryboard) {
            self.services = services
            self.navigationController = navigationController
            self.storyboard = storyBoard
    }
    
    func navigateToMap() {
        let viewController: MapViewController = storyboard
            .instantiateViewController()
        
        viewController.viewModel = MapViewModel(
            useCase: services.makeNavigationRouteUseCase(),
            navigator: self)
        navigationController.setNavigationBarHidden(false, animated: false)
        navigationController.pushViewController(viewController, animated: true)
    }
}
