//  Created by Paulius Cesekas on 13/04/2018.

import Domain

import UIKit
import Mapbox
import RxSwift
import RxCocoa

class MapViewController: UIViewController {
    private let disposeBag = DisposeBag()
    
    var viewModel: MapViewModel!
    private var regionList: Driver<[Region]>!
    private var mainRegion: Region?
    private var shouldFollowUser = false

    // MARK: - Outlets
    @IBOutlet private weak var mapView: MGLMapView!
    @IBOutlet private weak var downloadButton: UIBarButtonItem!
    @IBOutlet private weak var progressView: UIProgressView!

    // MARK: - Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    // MARK: - Configure
    private func configure() {
        guard viewModel != nil else {
            fatalError("View model is not set")
        }
        
        progressView.isHidden = true
        mapView.delegate = self
        
        bindViewModel()
    }

    // MARK: - UI Actions
    @IBAction
    private func touchUpInsideDownloadButton(_ downloadButton: UIBarButtonItem) {
        
    }
    
    @IBAction
    private func touchUpInsideRecenterButton(_ downloadButton: UIBarButtonItem) {
        mapView.userTrackingMode = .followWithHeading
    }
    
    // MARK: - View model binding
    private func bindViewModel() {
        let output = viewModel.transform(input: input)
        output.title.drive(onNext: { [unowned self] (title) in
            self.title = title
        }).disposed(by: disposeBag)
        
        output.downloadProgress.asDriver(onErrorJustReturn: 0.1).drive(onNext: { [unowned self] (progress) in
            switch progress {
            case 0.0:
                self.progressView.isHidden = true
            case 1.0:
                self.progressView.isHidden = true
                self.notifyDownloadComplete()
            default:
                self.progressView.isHidden = false
                self.progressView.progress = Float(progress)
            }
        }).disposed(by: disposeBag)

        output.regionList.drive(onNext: { [unowned self] (regionList) in
            guard let firstRegion = regionList.first else {
                return
            }
            
            self.mainRegion = firstRegion
            
            let center = CLLocationCoordinate2D.middlePointOfListMarkers(listCoords: [firstRegion.southWest, firstRegion.northEast])
            self.mapView.setCenter(center, zoomLevel: Double(firstRegion.zoom.minimum), animated: true)
        }).disposed(by: disposeBag)
        
        output.route.drive(onNext: { [unowned self] (route) in
            self.updateMarkers(for: route)
        }).disposed(by: disposeBag)
    }
    
    private var input: MapViewModel.Input {
        let downloadButtonTap = downloadButton.rx.tap
        downloadButtonTap.asDriver(onErrorJustReturn: ()).drive(onNext: { [unowned self] in
            self.downloadButton.isEnabled = false
        }).disposed(by: disposeBag)
        let input = MapViewModel.Input(
            downloadMapTrigger: downloadButtonTap.asDriver())
        return input
    }
    
    // MARK: - Helpers
    private func updateMarkers(for route: Route) {
        if let annotations = mapView.annotations {
            mapView.removeAnnotations(annotations)
        }
        
        var coordinates = [CLLocationCoordinate2D]()
        for checkpoint in route.checkpointList {
            coordinates.append(checkpoint.coordinate)
            let checkpointAnnotation = MGLPointAnnotation()
            checkpointAnnotation.coordinate = checkpoint.coordinate
            checkpointAnnotation.title = checkpoint.name
            mapView.addAnnotation(checkpointAnnotation)
        }
        let polyline = MGLPolyline(coordinates: &coordinates, count: UInt(coordinates.count))
        mapView.addAnnotation(polyline)
    }
    
    private func notifyDownloadComplete() {
        let alertController = UIAlertController(
            title: L10n.Mapviewcontroller.Alert.offlineMapDownloadComplete,
            message: nil,
            preferredStyle: .alert)
        alertController.addAction(UIAlertAction(
            title: L10n.Common.Button.ok,
            style: .default))
        present(alertController, animated: true)
    }
}

// MARK: - MGLMapViewDelegate
extension MapViewController: MGLMapViewDelegate {
    func mapView(_ mapView: MGLMapView, regionDidChangeWith reason: MGLCameraChangeReason, animated: Bool) {
        switch reason {
        case .programmatic:
            break
        default:
            mapView.userTrackingMode = .none
        }
    }
}
