//  Created by Paulius Cesekas on 13/04/2018.

import Foundation
import CoreLocation
import Domain
import RxSwift
import RxCocoa
import Mapbox

enum MapError: Error {
    case locationServiceDisabled
    case locationServiceDenied
    case receivedMapboxOfflinePackError
    case reachedMaximumAllowedMapboxTiles
    case unknown
    
    var localizedDescription: String {
        switch self {
        case .locationServiceDisabled:
            return "Location service disabled"
        case .locationServiceDenied:
            return "Location service denied"
        case .unknown:
            return "Unexpected error"
        case .receivedMapboxOfflinePackError:
            return "Received mapbox offline pack error"
        case .reachedMaximumAllowedMapboxTiles:
            return "Reached maximum allowed mapbox tiles"
        }
    }
}

final class MapViewModel: NSObject, ViewModelType {
    struct Input {
        let downloadMapTrigger: Driver<Void>
    }
    struct Output {
        let title: Driver<String>
        let regionList: Driver<[Region]>
        let route: Driver<Route>
        let userLocation: Driver<CLLocationCoordinate2D>
        let userDirection: Driver<CLLocationDirection>
        let error: Driver<Error>
        let downloadProgress: Driver<Double>
    }

    private let useCase: NavigationRouteUseCase
    private let navigator: NavigationRouteNavigator
    
    private let disposeBag = DisposeBag()
    
    private var locationManager: CLLocationManager!
    private var userLocation = Driver<CLLocationCoordinate2D>.just(kCLLocationCoordinate2DInvalid)
    private var userDirection = Driver<CLLocationDirection>.just(CLLocationDirection.infinity)
    private var error = Driver<Error>.never()
    private var regions = Driver<[Region]>.just([])
    private var downloadProgress = BehaviorRelay<Double>(value: 0)
    private var downloadProgressPerPack = [MGLOfflinePack:Double]()

    init(useCase: NavigationRouteUseCase, navigator: NavigationRouteNavigator) {
        self.useCase = useCase
        self.navigator = navigator
        
        super.init()
        
        setupLocationManager()
        setupOfflinePackNotificationHandlers()
    }
    
    func transform(input: Input) -> Output {
        regions = useCase.regions().asDriverOnErrorJustComplete()
        let route = regions.flatMap { [unowned self] (regions) -> Driver<Route> in
            guard let region = regions.last else {
                return Driver<Route>.empty()
            }
            let routes = self.useCase.routes(for: region)
            let route = routes.flatMap({ (routes) -> Driver<Route> in
                guard let route = routes.last else {
                    return Driver<Route>.empty()
                }
                return Driver<Route>.just(route)
            })
            return route.asDriverOnErrorJustComplete()
        }
        let title = route.flatMapLatest { (route) -> Driver<String> in
            return PublishRelay<String>.just(route.name).asDriver(onErrorJustReturn: "")
        }
        
        input.downloadMapTrigger.drive(onNext: { [unowned self] in
            self.downloadRegionsOffline()
        }).disposed(by: disposeBag)
        
        return Output(
            title: title,
            regionList: regions,
            route: route,
            userLocation: userLocation,
            userDirection: userDirection,
            error: error,
            downloadProgress: downloadProgress.asDriver(onErrorJustReturn: 0.0))
    }
    
    // MARK: - Setup
    private func setupLocationManager() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.activityType = .otherNavigation
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation

        guard CLLocationManager.locationServicesEnabled() else {
            let error = BehaviorRelay<Error>(value: MapError.locationServiceDisabled)
            self.error.drive(error)
                .disposed(by: disposeBag)
            return
        }
        
        switch (CLLocationManager.authorizationStatus()) {
        case .denied, .restricted:
            let error = BehaviorRelay<Error>(value: MapError.locationServiceDenied)
            self.error.drive(error).disposed(by: disposeBag)
        case .notDetermined:
            locationManager.requestAlwaysAuthorization()
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
            locationManager.startUpdatingHeading()
        }
    }
    
    // MARK: - Helpers
    
}

extension MapViewModel: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch (CLLocationManager.authorizationStatus()) {
        case .denied, .restricted, .notDetermined:
            let error = BehaviorRelay<Error>(value: MapError.locationServiceDenied)
            self.error.drive(error).disposed(by: disposeBag)
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
            locationManager.startUpdatingHeading()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let lastLocation = locations.last else {
            return
        }
        userLocation.drive(BehaviorRelay<CLLocationCoordinate2D>(value: lastLocation.coordinate))
            .disposed(by: disposeBag)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        userDirection.drive(BehaviorRelay<CLLocationDirection>(value: newHeading.magneticHeading))
            .disposed(by: disposeBag)
    }
}

// MARK: - Map download
extension MapViewModel: MGLMapViewDelegate {
    private static let userInfoKeyName = "name"
    
    private func setupOfflinePackNotificationHandlers() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(offlinePackProgressDidChange),
            name: NSNotification.Name.MGLOfflinePackProgressChanged,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(offlinePackDidReceiveError),
            name: NSNotification.Name.MGLOfflinePackError,
            object: nil)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(offlinePackDidReceiveMaximumAllowedMapboxTiles),
            name: NSNotification.Name.MGLOfflinePackMaximumMapboxTilesReached,
            object: nil)
    }

    private func downloadRegionsOffline() {
        regions.drive(onNext: { [unowned self] (regions) in
            for region in regions {
                self.startOfflinePackDownload(for: region)
            }
        }).disposed(by: disposeBag)
    }
    
    private func startOfflinePackDownload(for region: Region) {
        let styleUrl = URL(string: "mapbox://styles/mapbox/streets-v10")
        let bounds = MGLCoordinateBounds(
            sw: region.southWest,
            ne: region.northEast)
        let tilePyramidOfflineRegion = MGLTilePyramidOfflineRegion(
            styleURL: styleUrl,
            bounds: bounds,
            fromZoomLevel: Double(region.zoom.minimum),
            toZoomLevel: Double(region.zoom.maximum))

        // Store some data for identification purposes alongside the downloaded resources.
        let userInfo = [MapViewModel.userInfoKeyName: region.name]
        let context = NSKeyedArchiver.archivedData(withRootObject: userInfo)
        
        // Create and register an offline pack with the shared offline storage object.
        MGLOfflineStorage.shared()
            .addPack(for: tilePyramidOfflineRegion, withContext: context) { (pack, error) in
                guard error == nil else {
                    // The pack couldn’t be created for some reason.
                    print("Error: \(error?.localizedDescription ?? "unknown error")")
                    return
                }
                
                guard let pack = pack else {
                    print("Error: no pack to download")
                    return
                }
                
                // Start downloading.
                pack.resume()
            }
    }
    
    private func hasOfflinePack(for region: Region) -> Bool {
        guard let packs = MGLOfflineStorage.shared().packs else {
            return false
        }
        
        return packs.contains(where: { (offlinePack) -> Bool in
            guard let offlinePackRegion = offlinePack.region as? MGLTilePyramidOfflineRegion else {
                return false
            }
            if offlinePackRegion.bounds.ne.latitude == region.northEast.latitude
                && offlinePackRegion.bounds.ne.longitude == region.northEast.longitude
                && offlinePackRegion.bounds.sw.latitude == region.southWest.latitude
                && offlinePackRegion.bounds.sw.longitude == region.southWest.longitude
                && Int(offlinePackRegion.minimumZoomLevel) == region.zoom.minimum
                && Int(offlinePackRegion.maximumZoomLevel) == region.zoom.maximum {
                    return isCompleteProgress(offlinePack)
            }
            return false
        })
    }
    
    // MARK: - MGLOfflinePack notification handlers
    @objc
    func offlinePackProgressDidChange(notification: NSNotification) {
        guard let pack = notification.object as? MGLOfflinePack,
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: pack.context) as? [String: String] else {
                return
        }
        
        let packProgress = pack.progress
        let packProgressPercentage = progressPercentage(pack)
        updateProgress(for: pack)
        
        // TODO: temporary progress logging
        if isCompleteProgress(pack) {
            let byteCount = ByteCountFormatter.string(fromByteCount: Int64(packProgress.countOfBytesCompleted), countStyle: ByteCountFormatter.CountStyle.memory)
            print("Offline pack “\(userInfo[MapViewModel.userInfoKeyName] ?? "unknown")” completed: \(byteCount), \(packProgress.countOfResourcesCompleted) resources")
        } else {
            // Otherwise, print download/verification progress.
            print("Offline pack “\(userInfo[MapViewModel.userInfoKeyName] ?? "unknown")” has \(packProgress.countOfResourcesCompleted) of \(packProgress.countOfResourcesExpected) resources — \(packProgressPercentage * 100)%.")
        }
    }
    
    @objc
    func offlinePackDidReceiveError(notification: NSNotification) {
        if let pack = notification.object as? MGLOfflinePack,
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: pack.context) as? [String: String],
            let error = notification.userInfo?[MGLOfflinePackUserInfoKey.error] as? NSError {
                print("Offline pack “\(userInfo[MapViewModel.userInfoKeyName] ?? "unknown")” received error: \(error.localizedFailureReason ?? "unknown error")")
                let errorBehaviorRelay = BehaviorRelay<Error>(value: MapError.receivedMapboxOfflinePackError)
                self.error.drive(errorBehaviorRelay)
                    .disposed(by: disposeBag)
        }
    }
    
    @objc
    func offlinePackDidReceiveMaximumAllowedMapboxTiles(notification: NSNotification) {
        if let pack = notification.object as? MGLOfflinePack,
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: pack.context) as? [String: String],
            let maximumCount = (notification.userInfo?[MGLOfflinePackUserInfoKey.maximumCount] as AnyObject).uint64Value {
            print("Offline pack “\(userInfo[MapViewModel.userInfoKeyName] ?? "unknown")” reached limit of \(maximumCount) tiles.")
            let errorBehaviorRelay = BehaviorRelay<Error>(value: MapError.reachedMaximumAllowedMapboxTiles)
            self.error.drive(errorBehaviorRelay)
                .disposed(by: disposeBag)
        }
    }
    
    // MARK: - Helpers
    private func progressPercentage(_ offlinePack: MGLOfflinePack) -> Double {
        let progress = offlinePack.progress
        let completedResources = progress.countOfResourcesCompleted
        let expectedResources = progress.countOfResourcesExpected
        let progressPercentage = Double(completedResources) / Double(expectedResources)
        return progressPercentage
    }
    
    private func isCompleteProgress(_ offlinePack: MGLOfflinePack) -> Bool {
        let progress = offlinePack.progress
        let completedResources = progress.countOfResourcesCompleted
        let expectedResources = progress.countOfResourcesExpected
        return completedResources == expectedResources
    }
    
    private func updateProgress(for pack: MGLOfflinePack) {
        downloadProgressPerPack[pack] = progressPercentage(pack)
        
        var totalProgress = 0.0
        for progressPerPack in downloadProgressPerPack {
            totalProgress += progressPerPack.value
        }
        totalProgress /= Double(downloadProgressPerPack.count)
        downloadProgress.accept(totalProgress)
    }
}
