//
//  NetworkPlatform.h
//  NetworkPlatform
//
//  Created by Paulius Cesekas on 13/04/2018.
//  Copyright © 2018 cheskapac. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for NetworkPlatform.
FOUNDATION_EXPORT double NetworkPlatformVersionNumber;

//! Project version string for NetworkPlatform.
FOUNDATION_EXPORT const unsigned char NetworkPlatformVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <NetworkPlatform/PublicHeader.h>


